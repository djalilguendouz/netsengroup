﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EFGetStarted
{
    public class Comment
    {
        public int CommentId { get; set; }
        public DateTime CommentDate { get; set; }
        public string CommentContent { get; set; }

        public int PostId { get; set; }
        public Post Post { get; set; }

        public int AuthorId { get; set; }
        public Author Author { get; set; }

    }
}
