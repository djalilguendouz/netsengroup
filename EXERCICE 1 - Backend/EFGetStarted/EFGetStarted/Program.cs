﻿using System;
using System.Linq;
using EFGetStarted.DAO;

namespace EFGetStarted
{
    class Program
    {
        static void Main()
        {
            using (var db = new BloggingContext())
            {
                // Create
                Console.WriteLine("Inserting a new blog");
                Tools.createBlog(db, "http://blogs.msdn.com/adonet");

                // Read the first blog
                Console.WriteLine("Querying for a blog");
                var blog = Tools.ReadBlog(db);

                // Update
                Console.WriteLine("Updating the blog and adding a post");
                Tools.UpdateBlog(db, blog);

                // Delete
                Console.WriteLine("Delete the blog");
                Tools.DeleteBlog(db, blog);
            }
        }
    }
}