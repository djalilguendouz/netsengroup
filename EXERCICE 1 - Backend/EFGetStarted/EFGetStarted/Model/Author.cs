﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EFGetStarted
{
    public class Author
    {
        public int AuthorId { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Photo { get; set; }

        public List<Post> Posts { get; } = new List<Post>();
        public List<Comment> Comments { get; } = new List<Comment>();
    }
}
