﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EFGetStarted.DAO
{
    public class Tools
    {
        // Method for creating a blog
        public static void createBlog(BloggingContext db, string url)
        {
            db.Add(new Blog { Url = url });
            db.SaveChanges();
        }

        // Method to return the first blog
        public static Blog ReadBlog(BloggingContext db)
        {
           return db.Blogs.OrderBy(b => b.BlogId)
                     .First();
        }

        //Method to update a blog (add post)
        public static void UpdateBlog(BloggingContext db, Blog blog)
        {
            
            blog.Url = "https://devblogs.microsoft.com/dotnet";
            blog.Posts.Add(
                new Post
                {
                    Title = "Hello World",
                    Content = "I wrote an app using EF Core!"
                });
            db.SaveChanges();
        }

        //Method to delete a blog 
        public static void DeleteBlog(BloggingContext db, Blog blog)
        {
            db.Remove(blog);
            db.SaveChanges();
        }

    }
}
