﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Notes
{
    public class MainViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        Page page;

        private string _fileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "notes.txt");

        public string editorTxt;
        public MainViewModel(Page page) {
            this.page = page;
            ReadCommand = new Command(ReadTxt);
            SaveCommand = new Command(SaveTxt);
            DeleteCommand = new Command(DeleteTxt);
            ReadCommand.Execute(null);
        }

        public ICommand SaveCommand { get;  set; }
        public ICommand ReadCommand { get;  set; }
        public ICommand DeleteCommand { get;  set; }

        // Method to save text to file
        private void SaveTxt()
        {
            if (!String.IsNullOrEmpty(EditorTxt))
            {
                File.WriteAllText(_fileName, EditorTxt);
            }
        }

        // Method to read text from file
        private void ReadTxt()
        {
            if (File.Exists(_fileName))
            {
                EditorTxt = File.ReadAllText(_fileName);
            }
        }


        // Method to delete file from disk
        private async void DeleteTxt()
        {
            bool answer = await page.DisplayAlert("Confirm?", "Would you like delete text !", "Yes", "No");
            if (answer)
            {
                if (File.Exists(_fileName))
                {
                    File.Delete(_fileName);
                }
                EditorTxt = string.Empty;
            }

        }


        public String EditorTxt
        {
            set { 
                if (this.editorTxt !=value) {
                    this.editorTxt = value;
                    OnPropertyChanged("EditorTxt");}                
                }
            get { return this.editorTxt; }
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
